import jinja2
import yaml
import os
import yaml

def load_yml_data():
    """Load data from all .yml files"""
    EXT = ".yaml"
    data = {}
    for datafile in os.listdir("."):
        if datafile.endswith(EXT):
            print("Loading file {}".format(datafile))
            key = os.path.splitext(os.path.basename(datafile))[0]
            print("key: {}".format(key))
            data[key] = yaml.load(open(datafile))
    return data
def render(data, template_dir="templates"):
    """Render all templates in template_dir (defauilt templates)"""
    for root, dirs, files in os.walk(template_dir):
        for file_name in files:
            template_file = os.path.join(root, file_name)
            with open(template_file) as file_:
                template = jinja2.Template(file_.read())
            rel_file_name = os.path.join(
                    os.path.relpath(root, template_dir),
                    file_name)
            with open(rel_file_name, "w") as file_:
                file_.write(template.render(data))


if __name__=="__main__":
    data = load_yml_data()
    render(data)
