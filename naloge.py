#!/usr/bin/python3
import re
import argparse
import os
import sys

EXERCISE_RE = re.compile(r"\\begin{exercise}.*?\\end{exercise}", re.DOTALL)
TAGS_RE = re.compile(r"\\begin{tags}(.*?)\\end{tags}", re.DOTALL)
ID_RE = re.compile(r"\\begin{id}(.*?)\\end{id}", re.DOTALL)

def remove_comments(text):
    lines = text.split("\n")
    new_text = ""
    for line in lines:
        if line.strip().startswith("%"):
            continue
        new_text += line + "\n"
    return new_text

def split_tags(s):
    return list(map(lambda s: s.strip(), s.split(",")))

def find_tags(e):
    l = TAGS_RE.findall(e)
    if len(l)<1:
        tag_str = ""
    else:
        tag_str = ",".join(l)
    return split_tags(tag_str)

def find_id(e):
    l = ID_RE.findall(e)
    if len(l) < 1:
        return ""
    return l[0]

def find_topics(tags):
    return list(filter(lambda s: s.startswith('T'), tags))

def tags_include_all_tags(tags1, tags2):
    return all(tag in tags2 for tag in tags1)

def filter_by_tags(exercises, tags):
    if len(tags) < 1:
        return exercises
    return list(filter(lambda e: tags_include_all_tags(tags, find_tags(e)), exercises))

def write_to_files(exercises, dirname):
    "Write exercises into single files grouped by topic in directory dirname"
    for e in exercises:
        tags = find_tags(e)
        topics = find_topics(tags)
        id = find_id(e)
        if len(id) < 1:
            continue
        if len(topics)>0:
            topic = topics[0]
        else:
            topic = "Txy"
        topic_dirname = "{}/{}".format(dirname, topic)
        os.makedirs(topic_dirname, exist_ok=True)
        filename = "{}/{}.tex".format(topic_dirname, id)
        with open(filename, 'w') as fp:
            fp.write(e)



if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("-t", "--tags", type=str,
        help="Izberi le naloge z značkami. Značke naj bodo ločene z vejicami. Izbrane bodo naloge, ki vsebujejo vse značke")
    parser.add_argument("-s", "--split", type=str,
        help="Razdeli in shrani naloge tako, da je v vsaki datoteko ena naloga.")
    args = parser.parse_args()
    if args.tags:
        tags = split_tags(args.tags)
    else:
        tags = []
    text = remove_comments(sys.stdin.read())
    exercises = EXERCISE_RE.findall(text)
    print("% number of all exercises: ", len(exercises))
    exercises = filter_by_tags(exercises, tags)
    print("% number of exercises with tags {}: {}".format(", ".join(tags), len(exercises)))
    dirname = args.split
    if dirname:
        print("Saving individual files in {}".format(dirname))
        write_to_files(exercises, dirname)
        exit()
    for e in exercises:
        print(e)